# frozen_string_literal: true

module Changelog
  class MergeRequest
    NO_CHANGELOG_LABEL = "no changelog"

    attr_reader :iid, :title, :labels

    def initialize(iid, title, labels)
      @iid = iid
      @title = title
      @labels = labels || []
    end

    def changelog_entry?
      return false if labels.include?(NO_CHANGELOG_LABEL)

      changes_handbook?
    end

    def date
      return DateTime.new(1970, 1, 1).to_date unless merged_at.is_a?(String)

      DateTime.parse(merged_at).to_date
    end

    def merged_at
      gitlab_merge_request.merged_at
    end

    def to_s
      "- [!#{iid}](#{link}) #{title}"
    end

    def link
      "https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/#{iid}"
    end

    def author
      gitlab_merge_request.author.name
    end

    def changes
      return @changes if defined?(@changes)

      api_retry do
        @changes = Gitlab.merge_request_changes(Changelog::WWW_GITLAB_COM_PROJECT_ID, iid)&.to_h
      end

      @changes
    end

    private

    def changes_handbook?
      return false unless changes&.has_key?('changes')

      changes['changes'].any? do |change|
        change['new_path'].start_with?("source/handbook")
      end
    end

    def gitlab_merge_request
      api_retry do
        @gitlab_merge_request ||= Gitlab.merge_request(Changelog::WWW_GITLAB_COM_PROJECT_ID, iid)
      end
      @gitlab_merge_request
    end

    # API calls can fail to connect occasionally, which really shouldn't be a reason to fail entirely
    # Give them 5 goes, just to get past any transient network fail
    def api_retry
      tries = 0
      begin
        yield
      rescue Errno::ETIMEDOUT, Net::OpenTimeout => error # There may be more errors we should catch
        puts "Received a known retriable exception #{error.class}; have failed #{tries} times so far"
        retry if (tries += 1) < 5
        puts "Giving up after 5 retries"
        raise error
      # rubocop:disable Style/RescueStandardError
      rescue => error # Some other error: log the class but don't retry; we can investigate later
        puts "Received an #{error.class} exception; not retrying"
        raise error
      end
      # rubocop:enable Style/RescueStandardError
    end
  end
end
